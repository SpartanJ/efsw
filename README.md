# Repository moved to GitHub due to the removal of Mercurial support

## Please go to the [efsw GitHub Repository](https://github.com/SpartanJ/efsw/)

Bitbucket decided to remove support to Mercurial, so I decided to move all my
repositories to GitHub since I lost all trust in Bitbucket. Development will
continue there.
